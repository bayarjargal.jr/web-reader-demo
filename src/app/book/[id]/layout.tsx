"use client";
import React, { FC } from 'react';
import { ReaderProvider } from 'steppe-web-reader';
// import "~steppe-web-reader/dist/"
const ReaderLayout: FC<any> = ({ children }) => {
    return (
        <ReaderProvider>
            {children}
        </ReaderProvider>
    )
}

export default ReaderLayout;