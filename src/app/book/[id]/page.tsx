"use client";
import { books } from "@steppe/const/books";
import Head from "next/head";
import { useParams, useRouter } from "next/navigation";
import React, { useEffect, useRef, useState } from "react";
import { WebReader, useReaderSettings } from "steppe-web-reader";
import "steppe-web-reader/dist/style.css";

import useClickAwayListener from "@steppe/hooks/useClickAwayListener";
import { motion, AnimatePresence } from "framer-motion";

import { Metadata, ResolvingMetadata } from "next";


const BookPage = () => {
    const [open, setOpen] = useState<any>(false);
    const params: any = useParams();

    useEffect(() => {
        if (localStorage.theme === 'dark' || (!('theme' in localStorage) && window.matchMedia('(prefers-color-scheme: dark)').matches)) {
            document.documentElement.classList.add('dark')
        } else {
            document.documentElement.classList.remove('dark')
        }

        // Whenever the user explicitly chooses light mode
        localStorage.theme = 'light'

        // Whenever the user explicitly chooses dark mode
        localStorage.theme = 'dark'

        // Whenever the user explicitly chooses to respect the OS preference
        localStorage.removeItem('theme')
    }, [])
    return (
        <div>
            <WebReader
                onShare={() => setOpen(true)}
                baseUrl={books[params.id] && books[params.id].baseUrl}
                name={books[params.id] && books[params.id].name}
                onCreateAnnotation={() => { return true }}
                onDeleteAnnotation={() => { return true }}
                onUpdateAnnotation={() => { return true }}
            />
        </div>
    );
};
export default BookPage;
