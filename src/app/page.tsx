import { Book } from '@steppe/components'
import { books } from '@steppe/const/books'

export default function Home() {
  return (
    <div className='w-screen p-10 h-screen grid grid-cols-2 lg:grid-cols-6 gap-4'>
      {
        books.map((book: any, index: number) => {
          return <Book key={index} index={index} {...book} />
        })
      }
    </div>
  )
}
