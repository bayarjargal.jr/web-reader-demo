"use client";
import Image from 'next/image';
import { useRouter } from 'next/navigation';
import React, { FC } from 'react';

export interface BookProps {
    name: string
    image: string
    resource: string
    [key: string]: any
}

const Book: FC<BookProps> = ({ name, image, resource, author, type, index }) => {
    const router = useRouter();
    return (
        <div className="cursor-pointer flex flex-col  items-center h-[300px]" onClick={() => router.push("/book/" + index)}>
            <div className="mt-2 w-full px-5">
                <p className='mt-2 text-left font-bold'>{name}</p>
                <p className='w-full text-slate-600'>{author}</p>
                <p>{type}</p>

            </div>
        </div>
    )
}

export default Book;